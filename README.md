# Git TP utilisation locale - revenir en arrière faire une branche et un merge 

## Pré-requis 

Nous allons repartir du dépôt obtenu à l'issue du premier TP [https://gitlab.com/cmat_sisdev/git-tp-local-1](https://gitlab.com/cmat_sisdev/git-tp-local-1)

## Objectif du TP 

### Première partie : facile 

1. Reprendre le dépôt du [tp 1](https://gitlab.com/cmat_sisdev/git-tp-local-1)
2. Faire des modification dans l'un des fichiers déjà existant et faire un commit 
3. Revenir en arrière dans l'historique (premier commit du TP 1)
4. Créer une branche `bug-fix` pour pouvoir faire des modifications 
5. Dans cette branche : 
    - ajouter un fichier et faire un commit 
    - modifier un fichier existant (pas celui modifié en 2.) et faire un commit 
6. Revenir sur la branche `main` et afficher l'historique du dépôt sous la forme d'un graph 
7. Merger les modifications de `bug-fix` dans `main` 
8. Détruire la branche `bug-fix`


### Seconde partie : conflit 🥊

1. Revenir en arrière dans le dépôt à l'état de la fin de l'étape 2 de la première partie 
2. Créer une branche `new-bug-fix`
3. Dans cette branche 
    - ajouter un fichier et faire un commit 
    - modifier le **même** fichier que celui de l'étape 2 de la première partie et faire un commit 
6. Revenir sur la branche `main` et afficher l'historique du dépôt sous la forme d'un graph 
7. Merger les modifications de `new-bug-fix` dans `main` 💣️ il va a priori y avoir un conflit !! 
8. Résoudre le conflit et finir le merge 
9. Détruire la branche `new-bug-fix` 


## Aide : les commandes nécessaires

```
git checkout 
## ou 
git switch 

git add 
git commit 
git merge
git log  
```